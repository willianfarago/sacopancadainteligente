#include <SD.h>

File arq_dados;

const int led_pin_1 = 5;
const int led_pin_2 = 2;
const int led_pin_3 = 3;
const int impacto_pin_1 = A6;
const int impacto_pin_2 = A7;
const int impacto_pin_3 = A5;
const int impacto_minimo = 250;
const int num_sequencias = 4;

int impacto_1 = 0;
int impacto_2 = 0;
int impacto_3 = 0;

int cont_sequencias = 0;
boolean cartao_memoria = false;
  
void setup() {
  pinMode(led_pin_1, OUTPUT);
  pinMode(led_pin_2, OUTPUT);
  pinMode(led_pin_3, OUTPUT);
  
  Serial.begin(9600);
  Serial.println("Vamos treinar");
  
  randomSeed(analogRead(0));
  
  pinMode(10, OUTPUT);
  init_cartao();
  
  init_sequencia();
}

void loop() {  
  if(cont_sequencias<=num_sequencias){
    init_sequencia();
  }else if(cont_sequencias == num_sequencias + 1){
    arq_dados.close();
  }else
    return;
}

void executa_sequencia(){
  int num_impactos = random(3,6);
  int prox_impacto = 0;
  int forca_impacto = 0;
  unsigned long temp_init = 0;
  unsigned long temp_fim = 0;
  
  for(int i=0;i<num_impactos;i++){
    prox_impacto = random(3);
    if(prox_impacto==0){
      digitalWrite(led_pin_1, HIGH);
      temp_init = millis();
      forca_impacto = get_impacto(impacto_pin_1);
      temp_fim =millis();
      digitalWrite(led_pin_1, LOW);
    }else if(prox_impacto==1){
      digitalWrite(led_pin_2, HIGH);
      temp_init = millis();
      forca_impacto = get_impacto(impacto_pin_2);
      temp_fim =millis();
      digitalWrite(led_pin_2, LOW);
    }else{
      digitalWrite(led_pin_3, HIGH);
      temp_init = millis();
      forca_impacto = get_impacto(impacto_pin_3);
      temp_fim =millis();
      digitalWrite(led_pin_3, LOW);
    }
    grava_dados_impacto(forca_impacto, temp_fim - temp_init);
    delay(500);
  }
}

int get_impacto(int pin){
  int forca = 0;
  int forca_max = 0;
  boolean passou_minimo = false;
  
  while(true){
    forca = analogRead(pin);
    
    if(!passou_minimo){
      if(forca >= impacto_minimo){
        forca_max = forca;
        passou_minimo = true;
      }
    }else{
      if(forca >= forca_max)
        forca_max = forca;
      else
        break;
    }
    
    delay(5);
  }
  
  return forca_max;
}

void init_cartao(){
  if (!SD.begin(4)){
    Serial.println("Cartao Falhou!");
    cartao_memoria = false;
  }else{
    Serial.println("Cartao Ok!");
    cartao_memoria = true;
  }
  
  if(cartao_memoria){
    arq_dados = SD.open("dados2.txt", FILE_WRITE);
  
    if (arq_dados) {
      Serial.println("Arquivo dados.txt aberto!");
    } else {
      Serial.println("Arquivo dados.txt nao aberto!");
    }
  }
}

void grava_nome_sequencia(){
  if(cartao_memoria){
    arq_dados.print("Sequencia ");
    arq_dados.println(cont_sequencias);
  }
}

void grava_dados_impacto(int forca, int temp){
  if(cartao_memoria){
    arq_dados.print("Forca: ");
    arq_dados.print(forca);
    arq_dados.print("   Tempo: ");
    arq_dados.println(temp);
  }
}

void init_sequencia(){
  cont_sequencias++;
  
  for(int i=0;i<5;i++){
    digitalWrite(led_pin_1, HIGH);
    digitalWrite(led_pin_2, HIGH);
    digitalWrite(led_pin_3, HIGH);
    delay(300);
    digitalWrite(led_pin_1, LOW);
    digitalWrite(led_pin_2, LOW);
    digitalWrite(led_pin_3, LOW);
    delay(1000);
  }
  
  grava_nome_sequencia();
  executa_sequencia();
}
